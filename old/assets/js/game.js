var CustomPipeline2 = new Phaser.Class({
    Extends: Phaser.Renderer.WebGL.Pipelines.TextureTintPipeline
    , initialize: function CustomPipeline2(game) {
        Phaser.Renderer.WebGL.Pipelines.TextureTintPipeline.call(this, {
            game: game
            , renderer: game.renderer
            , fragShader: [
        "precision mediump float;",

        "uniform float     time;"
        , "uniform vec2      resolution;"
        , "uniform sampler2D uMainSampler;"
        , "uniform vec2      mouse;"
        , "varying vec2 outTexCoord;",

        "float noise(vec2 pos) {"
            , "return fract(sin(dot(pos, vec2(12.9898 - time,78.233 + time/20.0))) * 43758.5453);"
        , "}",

        "void main( void ) {",

            "//vec2 normalPos = gl_FragCoord.xy / resolution.xy;"
            , "vec2 normalPos = outTexCoord;"
            , "vec2 pointer = mouse / resolution;"
            , "float pos = (gl_FragCoord.y / resolution.y);"
            , "float posX = (gl_FragCoord.x / resolution.x);"
, //                "float mouse_dist = length(vec2((pointer.x - normalPos.x) * (resolution.x / resolution.y), pointer.y - normalPos.y));",
            "float distortion = clamp(1.0, 0.0, 1.0);",

            "pos -= (distortion * distortion) * 0.25;",

            "float c = sin(pos * 900.0 ) * 0.4 + 0.4;"
            , "c = pow(c, 0.2);"
            , "c *= 0.5;",

            "float band_pos = fract(time * 0.01) * 3.0 - 1.0;"
            , "c += clamp( (1.0 - abs(band_pos - pos) * 10.0), 0.0, 1.0) * 0.1;",

            "c += distortion * 0.08;"
            , "// noise"
            , "c += (noise(gl_FragCoord.xy) - 0.5) * (0.09);",

            "vec4 pixel = texture2D(uMainSampler, outTexCoord);"
, //                "gl_FragColor = (sin(3.14*(gl_FragCoord.x/resolution.x)) * sin(3.14*(gl_FragCoord.y/resolution.y))) * (pixel + vec4(c/2.0, c/2.0, c/2.0, 1)) + (1.0 - sin(3.14*(gl_FragCoord.x/resolution.x)) * sin(3.14*(gl_FragCoord.y/resolution.y))) * pixel;",
            "gl_FragColor = sin(3.14*(gl_FragCoord.x/resolution.x)) * sin(3.15*((gl_FragCoord.y + 25.0) /resolution.y)) * (pixel + vec4(c/3.0, c/3.0, c/3.0, 1)/3.0);"
        , "}"

        ].join('\n')
        });
    }
});
class AnimatedParticle extends Phaser.GameObjects.Particles.Particle {
    constructor(emitter) {
        super(emitter);
        this.t = 0;
        this.i = 0;
    }
    update(delta, step, processors) {
        var result = super.update(delta, step, processors);
        this.t += delta;
        if (this.t >= anim.msPerFrame) {
            this.i++;
            if (this.i > 11) {
                this.i = 0;
            }
            this.frame = anim.frames[this.i].frame;
            this.t -= anim.msPerFrame;
        }
        return result;
    }
}
var config = {
    type: Phaser.FIT
    , width: 960
    , height: 540
    , scale: {
        mode: Phaser.Scale.WIDTH_CONTROLS_HEIGHT
        , autoCenter: Phaser.Scale.CENTER_BOTH
    , }
    , physics: {
        default: 'arcade'
        , arcade: {
            gravity: {
                y: 600
            }
            , debug: false
        }
    }
    , scene: {
        preload: preload
        , create: create
        , update: update
    }
    , parent: 'game'
    , transparent: true
    , autoRound: true
};
var month = new Array();
month[0] = "January";
month[1] = "February";
month[2] = "March";
month[3] = "April";
month[4] = "May";
month[5] = "June";
month[6] = "July";
month[7] = "August";
month[8] = "September";
month[9] = "October";
month[10] = "November";
month[11] = "December";
var weekday = new Array(7);
weekday[0] = "Sunday";
weekday[1] = "Monday";
weekday[2] = "Tuesday";
weekday[3] = "Wednesday";
weekday[4] = "Thursday";
weekday[5] = "Friday";
weekday[6] = "Saturday";
let anim;
let flowerEmitter;
var scene = {};
var player;
var game = new Phaser.Game(config);
var stars1;
var globalColor;
var currentColor = 255;
var hearts;
var emitter;
var pointer;
var touching = false;
var currentStringTarget = "";
var typeTime = 50;
var jumpCD = 0;
var swiping = false;
var camera;
var swipeVelocityX = 0;
var keys;
window.mobileAndTabletcheck = function () {
    var check = false;
    (function (a) {
        if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true;
    })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
};

function datePrefix(d) {
    if (d > 3 && d < 21) return 'th';
    switch (d % 10) {
    case 1:
        return "st";
    case 2:
        return "nd";
    case 3:
        return "rd";
    default:
        return "th";
    }
}

function preload() {
    //Load our assets here
    //        this.load.image('logo', 'assets/lazymondaylogo.png');
    this.load.image('rtd', 'assets/r_t_d.png');
    this.load.image('heart', 'assets/heart.png');
    this.load.image('ground', 'assets/ground.png');
    this.load.image('platform', 'assets/platform.png');
    this.load.image('playerCollider', 'assets/player_collider.png');
    this.load.image('treesBG', 'assets/trees_bg.png');
    this.load.image('grassFG', 'assets/grass_bg.png');
    this.load.image('hillsBG', 'assets/hills_bg.png');
    this.load.spritesheet('playerIdle', 'assets/player_idle.png', {
        frameWidth: 24
        , frameHeight: 54
    });
    this.load.spritesheet('playerJump', 'assets/player_jump.png', {
        frameWidth: 24
        , frameHeight: 54
    });
    this.load.spritesheet('playerWalk', 'assets/player_walk.png', {
        frameWidth: 24
        , frameHeight: 54
    });
    this.load.spritesheet('playerLookup', 'assets/player_lookup.png', {
        frameWidth: 24
        , frameHeight: 54
    });
    this.load.spritesheet('flowerGrow', 'assets/white_daisy.png', {
        frameWidth: 32
        , frameHeight: 44
    });
    customPipeline = game.renderer.addPipeline('Custom', new CustomPipeline2(game));
    customPipeline.setFloat2('resolution', game.config.width, game.config.height);
    customPipeline.setFloat2('mouse', 0.0, 0.0);
}

function getGreeting() {
    var today = new Date()
    var curHr = today.getHours()
    if (curHr < 12) {
        return "~/" + weekday[today.getDay()] + " morning, " + month[today.getMonth()] + " " + today.getDate() + datePrefix(today.getDate()) + ".";
    }
    else if (curHr < 18) {
        return "~/" + weekday[today.getDay()] + " afternoon, " + month[today.getMonth()] + " " + today.getDate() + datePrefix(today.getDate()) + ".";
    }
    else {
        return "~/" + weekday[today.getDay()] + " evening, " + month[today.getMonth()] + " " + today.getDate() + datePrefix(today.getDate()) + ".";
    }
}

function create() {
    platforms = this.physics.add.staticGroup();
    platforms.create(480, 580, 'ground').setScale(2, 1).refreshBody();
    platforms.create(960 + 200, 500, 'platform').setScale(.4, .7).refreshBody();
    platforms.create(960 + 200, 470, 'platform').setScale(.4, .2).refreshBody();
    platforms.create(960 + 825, 400, 'platform').setScale(1, .2).refreshBody();
    platforms.create(960 + 500, 450, 'platform').setScale(2, .2).refreshBody();
    platforms.create(960 + 560, 365, 'platform').setScale(.5, .2).refreshBody();
    platforms.create(960 + 100, 330, 'platform').setScale(3, .2).refreshBody();
    platforms.create(960 + 0, 250, 'platform').setScale(.5, .2).refreshBody();
    platforms.create(960 + 175, 200, 'platform').setScale(.4, .2).refreshBody();
    platforms.create(960 + 750, 150, 'platform').setScale(4, .2).refreshBody();
    dialogues = this.physics.add.staticGroup();
    currentStringTarget = "~\\BOOTING...\n~\\CONTROL UNIT FOUND\n~\\".concat(!mobileAndTabletcheck() ? "ARROWS TO ACTUATE>" : "");
    var d = dialogues.create(960 + 200, 499, 'platform').setScale(.4, .21).refreshBody();
    d.associatedText = getGreeting().toUpperCase() + "\n~/SYSTEMS STATUS OK \n~/LZM STATUS OK";
    var d = dialogues.create(960 + 100, 329, 'platform').setScale(3, .21).refreshBody();
    d.associatedText = "~\\SCANNING LIFE SIGNS...\n~\\...           \n~\\0 found.";
    var d = dialogues.create(960 + 750, 149, 'platform').setScale(4, .2).refreshBody();
    d.associatedText = "~\\STAR SYSTEM:LONELY \n~\\ACTIVATING LOCAL SYSTEM COMMS\n~\\AWAITING TARGET ADDRESS...";
    player = this.physics.add.sprite(480, 500, 'playerCollider');
    player.setBounce(0);
    player.setCollideWorldBounds(true);
    var treesA = this.add.image(480, 267, 'treesBG');
    treesA.scrollFactorX = 1.1;
    treesA.scrollFactorY = 0;
    treesA.setDepth(3);
    var treesA = this.add.image(240, 270, 'treesBG');
    treesA.scrollFactorX = .9;
    treesA.scrollFactorY = 0;
    treesA.setScale(-1, 1);
    treesA.tint = 0x000000;
    treesA.setDepth(-1);
    var grassA = this.add.image(960, 270, 'grassFG');
    grassA.scrollFactorX = 0.9;
    grassA.scrollFactorY = 0;
    grassA.setDepth(-1);
    var grassB = this.add.image(500, 270, 'grassFG');
    grassB.setScale(-1, 1);
    var hillsA = this.add.image(500, 240, 'hillsBG');
    hillsA.setDepth(-5);
    hillsA.setScale(1.2, 1.2);
    hillsA.scrollFactorX = 0.5;
    hillsA.scrollFactorY = 0;
    var hillsB = this.add.image(500, 240, 'hillsBG');
    hillsB.setDepth(-5);
    hillsB.setScale(-1.2, 1.2);
    hillsB.scrollFactorX = 0.5;
    hillsB.scrollFactorY = 0;
    hillsB.tint = 0x000000;
    
    this.physics.add.collider(player, platforms, hitCollider);
    this.physics.add.overlap(player, dialogues, hitDialogue, null, this);
    this.physics.world.setBounds(0, 0, 1920, 960);
    camera = this.cameras.main;
    camera.startFollow(player, false, 0.01, 0, 0, 235);
    this.cameras.main.setRenderToTexture(customPipeline);
    //Player:
    this.anims.create({
        key: 'idle'
        , frames: this.anims.generateFrameNumbers('playerIdle', {
            start: 0
            , end: 9
        })
        , frameRate: 10
        , repeat: -1
    });
    this.anims.create({
        key: 'walk'
        , frames: this.anims.generateFrameNumbers('playerWalk', {
            start: 0
            , end: 8
        })
        , frameRate: 10
        , repeat: -1
    });
    this.anims.create({
        key: 'lookUp'
        , frames: this.anims.generateFrameNumbers('playerLookup', {
            start: 1
            , end: 9
        })
        , frameRate: 6
        , repeat: -1
    });
    this.anims.create({
        key: 'jumpUp'
        , frames: this.anims.generateFrameNumbers('playerJump', {
            start: 0
            , end: 0
        })
        , frameRate: 6
        , repeat: -1
    });
    this.anims.create({
        key: 'jumpDown'
        , frames: this.anims.generateFrameNumbers('playerJump', {
            start: 1
            , end: 1
        })
        , frameRate: 6
        , repeat: -1
    });
    hearts = this.add.particles('heart');
    emitter = hearts.createEmitter();
    emitter.setPosition(-200, -200);
    emitter.gravityY = -100;
    emitter.life = 300;
    emitter.emitCallback = Emitted;
    

//    let config = {
//        key: 'flowerGrow'
//        , frames: this.anims.generateFrameNumbers('flowerGrow')
//        , frameRate: 4
//        , repeat: -1
//    };
//    anim = this.anims.create(config);
//    let particles = this.add.particles('flowerGrow');
//    flowerEmitter = particles.createEmitter({
//        x: 100
//        , y: 100
//        , frame: 0
//        , quantity: 100
//        , frequency: 200
//            //        , angle: {
//            //            min: 0
//            //            , max: 30
//            //        }
//            
//        , scale: 1
//            //        , speed: 200
//            
//        , gravityY: 0
//        , lifespan: 2400
//        , particleClass: AnimatedParticle
//    });
//    //    flowerEmitter.scrollFactorX = 0.9;
//    flowerEmitter.stop();
    player.setDepth(1);
    cursors = this.input.keyboard.createCursorKeys();
    keys = this.input.keyboard.addKeys('W,S,A,D');

    pointer = this.input.activePointer;
    var gameElement = document.getElementById('game');
    var mc = new Hammer(gameElement);
    var tapper = new Hammer.Manager(gameElement);
    tapper.add(new Hammer.Tap({
        event: 'doubletap'
        , taps: 2
    }));
    // Single tap recognizer
    tapper.add(new Hammer.Tap({
        event: 'singletap'
    }));
    mc.get('swipe').set({
        direction: Hammer.DIRECTION_ALL
    });
    //    mc.on("tap", function (ev) {
    //        swiping = true;
    //    });
    //    mc.on("doubletap", function (ev) {
    //        if (player.body.touching.down) {
    //            player.setVelocityY(-320);
    //        }
    //    });
    // Single tap recognizer
    tapper.add(new Hammer.Tap({
        event: 'singletap'
    }));
    tapper.on("singletap", function (ev) {
        if (ev.type == "singletap" && player.body.touching.down) {
            player.setVelocityY(-320);
        }
    });
    mc.on("swipe", function (ev) {
        swiping = true;
        if (ev.velocityX > 1) {
            if (swipeVelocityX < -1) {
                swipeVelocityX = 0;
            }
            else {
                swipeVelocityX = 100;
            }
        }
        if (ev.velocityX < -1) {
            if (swipeVelocityX > 1.0) {
                swipeVelocityX = 0;
            }
            else {
                swipeVelocityX = -100;
            }
        }
    });
    
    
    this.input.keyboard.disableGlobalCapture();
    
}

function Emitted() {
    emitter.stop();
}

function update(time, delta) {
    if (currentColor > globalColor) {
        currentColor -= 0.25;
        document.getElementById("page").style.backgroundColor = 'rgba('.concat((currentColor < 1) ? 3 : currentColor).concat(', ').concat((currentColor < 1) ? 8 : currentColor).concat(', ').concat((currentColor < 1) ? 15 : currentColor).concat(', 1)');
        
    }
   
    
    
    

    if(cursors.left.isDown || cursors.right.isDown || cursors.up.isDown || cursors.down.isDown ||
      keys.A.isDown || keys.D.isDown || keys.W.isDown|| keys.S.isDown){
        swiping = false;
    }
    
    if (swiping == false) {
        if (cursors.left.isDown || keys.A.isDown) {
            player.setVelocityX(-100);
            player.setScale(-1, 1);
            player.body.offset.x = 24;
            player.anims.play('walk', true);
        }
        else if (cursors.right.isDown || keys.D.isDown) {
            player.setScale(1, 1);
            player.setVelocityX(100);
            player.body.offset.x = 0;
            player.anims.play('walk', true);
        }
        else {
            player.setVelocityX(0);
            if (player.y < 150) {
                player.anims.play('lookUp', true);
            }
            else {
                player.anims.play('idle', true);
            }
        }
        if ((cursors.up.isDown || keys.W.isDown) && player.body.touching.down) {
            player.setVelocityY(-320);
        }
    }
    else {
        if (swipeVelocityX > 0) {
            player.setScale(1, 1);
        }
        else if (swipeVelocityX < 0) {
            player.setScale(-1, 1);
            player.body.offset.x = 24;
        }
        else {
            player.anims.play('idle', true);
        }
        player.setVelocityX(swipeVelocityX);
    }
    if (Math.abs(player.body.velocity.x) > 0) {
        player.anims.play('walk', true);
    }
    if (player.body.velocity.y > 10) {
        player.anims.play('jumpDown', true);
    }
    else if (player.body.velocity.y < -10) {
        player.anims.play('jumpUp', true);
    }
    if (currentStringTarget != "" && (currentStringTarget != document.getElementById("text").innerHTML)) {
        if (typeTime <= 0) {
            typeTime = 50;
            document.getElementById("text").innerHTML = currentStringTarget.substring(0, document.getElementById("text").innerHTML.length + 1);
        }
        else {
            typeTime -= delta;
        }
    }
    if (jumpCD > 0) {
        jumpCD -= delta;
    }
    customPipeline.setFloat1('time', time);
//    if (player.x < 1440 && (player.body.velocity.x != 0) && player.y > 500) {
//        if (Math.random() > 0.97 && player.body.touching.down) {
//            flowerEmitter.setPosition(player.x + (Math.random() - 0.5) * 4, player.y + 10);
//            flowerEmitter.emitParticle(1);
//        }
//    }
    if (player.x > 1440) {
        //        camera.stopFollow(player);
    }
}

function rgbToHex(r, g, b) {
  return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}


function emitAtPlayer() {
    emitter.setPosition(player.x, player.y - 45);
    emitter.setSpeed(50);
    emitter.setScale(1.50);
    emitter.setBlendMode(Phaser.BlendModes.ADD);
    emitter.emitParticle(3);
}

function lerp(start, end, amt) {
    return (1 - amt) * start + amt * end
}

function hitDialogue(player, dialogue) {
    document.getElementById("text").innerHTML = "";
    typeTime = 250;
    currentStringTarget = dialogue.associatedText;
    dialogue.disableBody(true, true);
    if (dialogue.associatedText.includes("~\\STAR SYSTEM:LONELY")) {
        document.getElementById("subscribeButton").className += " blink-image0";
        document.getElementById("userEmail").className += " blink-image0";
        document.getElementById("userName").className += " blink-image0";
    }
}

function hitCollider() {
    if (player.y < 250) {
        globalColor = Math.round((player.y * 1.2) - 150);
        document.getElementById("stars").style.display = 'block';
        if (player.y < 150) {
            document.getElementById("text").style.color = 'white';
        }
    }
    else {
        document.getElementById("text").style.color = 'black';
    }
    //        }
    //        else {
    //            globalColor = 255;
    //            document.getElementById("stars").style.display = 'none';
    //            
    //        }
}